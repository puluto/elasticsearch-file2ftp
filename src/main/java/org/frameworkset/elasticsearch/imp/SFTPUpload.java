package org.frameworkset.elasticsearch.imp;

import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.sftp.SFTPClient;
import net.schmizz.sshj.transport.verification.PromiscuousVerifier;
import net.schmizz.sshj.xfer.FileSystemFile;
import org.frameworkset.runtime.CommonLauncher;

import java.io.IOException;

/** This example demonstrates uploading of a file over SFTP to the SSH server. */
public class SFTPUpload {

    public static void main(String[] args)
            throws IOException {
        String src = CommonLauncher.getProperty("src","F:\\4_ASIA文档\\1_项目\\7_湖南移动\\01工程项目\\2020\\可视化运维\\用户新业务需求\\湖南移动新业务项目(关于将本省电渠业务办理失败日志推送在线开展回捞营销的需求)软件需求规格说明书(1).docx");//同时指定了默认值
        String downDir = CommonLauncher.getProperty("downDir","d:/temp");//同时指定了默认值

        final SSHClient ssh = new SSHClient();

        ssh.addHostKeyVerifier(new PromiscuousVerifier());
//        ssh.loadKnownHosts();
//        ssh.addHostKeyVerifier("2a:da:5a:6a:cf:7d:65:e5:ac:ff:d3:73:7f:2c:55:c9");
        ssh.connect("10.13.6.127",5322);
        // only for public key authentication
//        ssh.authPublickey("user", "location to private key file");
        try {
//            ssh.addHostKeyVerifier(new PromiscuousVerifier());
            ssh.authPassword("ecs","ecs@123");
//            final String src = "F:\\4_ASIA文档\\1_项目\\7_湖南移动\\01工程项目\\2020\\可视化运维\\用户新业务需求\\湖南移动新业务项目(关于将本省电渠业务办理失败日志推送在线开展回捞营销的需求)软件需求规格说明书(1).docx";
            final SFTPClient sftp = ssh.newSFTPClient();
            try {
                sftp.put(new FileSystemFile(src), "/home/ecs/failLog");
                sftp.get("/home/ecs/failLog/elasticsearch-file2ftp-6.3.5-released.zip", new FileSystemFile(downDir));
            } finally {
                sftp.close();
            }
        } finally {
            ssh.disconnect();
        }
    }

}
